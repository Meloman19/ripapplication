﻿using RIPApplication.Classes.Services;

namespace RIPApplication
{
    public static class ServiceInjector
    {
        public static void InjectServices()
        {
            ServiceContainer.Instance.AddService<IMessageBoxService>(new MessageBoxService());
            ServiceContainer.Instance.AddService<IAddEditService>(new AddEditService());
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace RIPApplication
{
    public class ServiceContainer
    {
        public static readonly ServiceContainer Instance = new ServiceContainer();

        readonly Dictionary<Type, object> _serviceDict;
        readonly object _serviceDictLock;

        private ServiceContainer()
        {
            _serviceDict = new Dictionary<Type, object>();
            _serviceDictLock = new object();
        }

        public void AddService<TService>(TService implementation)
            where TService : class
        {
            lock (_serviceDictLock)
            {
                _serviceDict[typeof(TService)] = implementation;
            }
        }

        public TService GetService<TService>()
            where TService : class
        {
            lock (_serviceDictLock)
            {
                _serviceDict.TryGetValue(typeof(TService), out object service);
                return service as TService;
            }
        }

    }
}
﻿using RIPApplication.Classes;
using RIPApplication.Model;

namespace RIPApplication.ViewModel
{
    class MainWindowVM : BindingObject
    {
        public CompanyDataGridVM CompanyDataGrid { get; } = new CompanyDataGridVM();

        public UserDataGridVM UserDataGrid { get; } = new UserDataGridVM();

        public MainWindowVM()
        {
            CompanyDataGrid.Selected += company => UserDataGrid.OpenCompany(company);
        }
    }
}
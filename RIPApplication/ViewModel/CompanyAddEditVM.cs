﻿using RIPApplication.Classes;
using RIPApplication.Classes.Services;
using RIPApplication.Model;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RIPApplication.ViewModel
{
    class CompanyAddEditVM : BindingObject
    {
        #region Private

        private enum Status
        {
            Add,
            Edit
        }

        private readonly Status status;
        private Company company;
        private bool isEnabled = true;

        #endregion

        #region Properties

        public string Name
        {
            get => company.Name;
            set => company.Name = value;
        }

        public ContractStatus ContractStatus
        {
            get => company.ContractStatus;
            set => company.ContractStatus = value;
        }

        public ICommand AddEdit { get; }

        public string AddEditText => status == Status.Add ? "Добавить" : "Изменить";

        public string Title => status == Status.Add ? "Добавление компании" : "Изменение свойств компании";

        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                if (isEnabled == value)
                {
                    isEnabled = value;
                    Notify(nameof(IsEnabled));
                }
            }
        }

        #endregion

        public CompanyAddEditVM()
        {
            this.company = new Company();
            status = Status.Add;
            AddEdit = new CommandAsync<ICloseable>(obj => AddCompany(obj));
        }

        public CompanyAddEditVM(Company company)
        {
            this.company = company;
            status = Status.Edit;
            AddEdit = new CommandAsync<ICloseable>(obj => UpdateCompany(obj));
        }

        private async Task AddCompany(ICloseable obj)
        {
            IsEnabled = false;

            try
            {
                await company.Add();
                if (obj != null)
                    obj.Close();
            }
            catch
            {
                ServiceContainer.Instance.GetService<IMessageBoxService>()
                      .Show("Не удалось добавить компанию.",
                      "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }

            IsEnabled = true;
        }

        private async Task UpdateCompany(ICloseable obj)
        {
            IsEnabled = false;

            try
            {
                await company.Update();
                if (obj != null)
                    obj.Close();
            }
            catch
            {
                ServiceContainer.Instance.GetService<IMessageBoxService>()
                       .Show("Не удалось изменить параметры компания\nВозможно, данная компания уже удалена.",
                       "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }

            IsEnabled = true;
        }
    }
}
﻿using RIPApplication.Classes;
using RIPApplication.Model;
using RIPApplication.View;
using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RIPApplication.ViewModel
{
    class CompanyDataGridVM : BindingObject
    {
        public event Action<Company> Selected;

        #region Fields

        private readonly CompanyWorker companyWorker = new CompanyWorker();

        private bool isLoaded = false;

        #endregion

        #region Properties

        public ReadOnlyObservableCollection<Company> Companies { get; }

        public ICommand CompanyAdd { get; }

        public ICommand CompanyEdit { get; }

        public ICommand CompanyDelete { get; }

        public ICommand Refresh { get; }

        public bool IsLoaded
        {
            get => isLoaded;
            set
            {
                if (isLoaded != value)
                {
                    isLoaded = value;
                    Notify(nameof(IsLoaded));
                }
            }
        }

        public Company SelectedItem
        {
            set
            {
                Selected?.Invoke(value);
            }
        }

        #endregion

        public CompanyDataGridVM()
        {
            Companies = new ReadOnlyObservableCollection<Company>(companyWorker.companies);

            CompanyAdd = new CommandRelay(() => companyWorker.AddCompany());
            CompanyEdit = new CommandRelay<Company>(company => companyWorker.EditCompany(company));
            CompanyDelete = new CommandAsync<Company>(company => companyWorker.DeleteCompany(company));
            Refresh = new CommandRelay(() => companyWorker.RefreshCollection());

            companyWorker.Loading += () => IsLoaded = false;
            companyWorker.Loaded += () => IsLoaded = true;

            companyWorker.RefreshCollection();
        }
    }
}
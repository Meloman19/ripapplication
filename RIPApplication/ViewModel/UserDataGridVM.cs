﻿using RIPApplication.Classes;
using RIPApplication.Model;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace RIPApplication.ViewModel
{
    class UserDataGridVM : BindingObject
    {
        #region Fields

        private UserWorker userWorker = new UserWorker();

        private bool isLoaded = true;

        #endregion

        #region Properties

        public ReadOnlyObservableCollection<User> Users { get; }

        public ICommand UserAdd { get; }

        public ICommand UserEdit { get; }

        public ICommand UserDelete { get; }

        public ICommand Refresh { get; }

        public bool IsLoaded
        {
            get => isLoaded;
            set
            {
                if (isLoaded != value)
                {
                    isLoaded = value;
                    Notify(nameof(IsLoaded));
                }
            }
        }

        #endregion

        public UserDataGridVM()
        {
            Users = new ReadOnlyObservableCollection<User>(userWorker.users);

            userWorker.Loading += () => IsLoaded = false;
            userWorker.Loaded += () => IsLoaded = true;

            UserAdd = new CommandRelay(userWorker.AddUser);
            UserEdit = new CommandRelay<User>(userWorker.EditUser);
            UserDelete = new CommandAsync<User>(async user => await userWorker.DeleteUser(user));
            Refresh = new CommandRelay(() => userWorker.RefreshCollection());
        }

        public void OpenCompany(Company company) => userWorker.RefreshCollection(company);
    }
}
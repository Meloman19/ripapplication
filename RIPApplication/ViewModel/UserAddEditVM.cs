﻿using RIPApplication.Classes;
using RIPApplication.Classes.Services;
using RIPApplication.Model;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RIPApplication.ViewModel
{
    class UserAddEditVM : BindingObject
    {
        #region Private

        private enum Status
        {
            Add,
            Edit
        }

        private readonly Status status;
        private readonly Company company;
        private User user;
        private bool isEnabled = true;

        #endregion

        #region Properties

        public string Name
        {
            get => user.Name;
            set => user.Name = value;
        }

        public string Login
        {
            get => user.Login;
            set => user.Login = value;
        }

        public string Password
        {
            get => user.Password;
            set => user.Password = value;
        }

        public ICommand AddEdit { get; }

        public string AddEditText => status == Status.Add ? "Добавить" : "Изменить";

        public string Title => status == Status.Add ? "Добавление пользователя" : "Изменение свойств пользователя";

        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                if (isEnabled == value)
                {
                    isEnabled = value;
                    Notify(nameof(IsEnabled));
                }
            }
        }

        #endregion

        public UserAddEditVM(Company company)
        {
            this.company = company ?? throw new ArgumentNullException(nameof(company));
            this.user = new User();
            status = Status.Add;
            AddEdit = new CommandAsync<ICloseable>(obj => AddUser(obj));
        }

        public UserAddEditVM(User user)
        {
            this.user = user ?? throw new ArgumentNullException(nameof(user));
            status = Status.Edit;
            AddEdit = new CommandAsync<ICloseable>(obj => UpdateUser(obj));
        }

        private async Task AddUser(ICloseable obj)
        {
            IsEnabled = false;

            try
            {
                await user.Add(company);
                if (obj != null)
                    obj.Close();
            }
            catch
            {
                ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Не удалось добавить пользователя",
                        "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }

            IsEnabled = true;
        }

        private async Task UpdateUser(ICloseable obj)
        {
            IsEnabled = false;

            try
            {
                await user.Update();
                if (obj != null)
                    obj.Close();
            }
            catch
            {
                ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Не удалось изменить параметры пользователя\nВозможно, данный пользователь уже удалён.",
                        "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }

            IsEnabled = true;
        }
    }
}
﻿using System.Data.Entity;
using System.Threading.Tasks;

namespace RIPApplication.Model
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public Company Company { get; set; }

        #region Public Methods

        public async Task Add(Company company)
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Companies.Attach(company).Users.Add(this);
                await companyContext.SaveChangesAsync();
            }
        }

        public async Task Update()
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Entry<User>(this).State = EntityState.Modified;
                await companyContext.SaveChangesAsync();
            }
        }

        public async Task Delete()
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Users.Attach(this);
                companyContext.Users.Remove(this);
                await companyContext.SaveChangesAsync();
            }
        }

        #endregion
    }
}
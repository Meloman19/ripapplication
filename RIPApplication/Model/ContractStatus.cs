﻿using System.ComponentModel;

namespace RIPApplication.Model
{
    public enum ContractStatus : int
    {
        [Description("Ещё не заключён")]
        NotConcluded = 0,
        [Description("Заключён")]
        Concluded = 1,
        [Description("Расторгнут")]
        Broken = 2
    }
}
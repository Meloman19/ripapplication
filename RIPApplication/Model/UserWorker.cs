﻿using RIPApplication.Classes;
using RIPApplication.Classes.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace RIPApplication.Model
{
    class UserWorker
    {
        #region Events

        public event Action Loading;

        public event Action Loaded;

        #endregion

        private Company currentCompany;

        public ObservableCollection<User> users = new ObservableCollection<User>();

        #region Public Methods

        public void RefreshCollection()
        {
            Loading?.Invoke();
            users.Clear();

            if (currentCompany != null)
            {
                using (var ripContext = new RIPContext())
                {
                    currentCompany = ripContext.Companies.FirstOrDefault(x => x.Id == currentCompany.Id);

                    if (currentCompany != null)
                    {
                        foreach (var user in currentCompany.Users)
                            users.Add(user);
                    }
                }
            }

            Loaded?.Invoke();
        }

        public void RefreshCollection(Company company)
        {
            if (currentCompany != company)
            {
                currentCompany = company;
                RefreshCollection();
            }
        }

        public void AddUser()
        {
            if (currentCompany != null)
            {
                ServiceContainer.Instance.GetService<IAddEditService>().UserShow(currentCompany);
                RefreshCollection();
            }
        }

        public void EditUser(User user)
        {
            if (user != null)
            {
                ServiceContainer.Instance.GetService<IAddEditService>().UserShow(user);
                RefreshCollection();
            }
        }

        public async Task DeleteUser(User user)
        {
            if (user != null)
            {
                try
                {
                    var result = ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Вы действительно хотите удалить пользователя из базы?", "Внимание", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question);
                    if (result != System.Windows.MessageBoxResult.Yes)
                        return;

                    await user.Delete();
                }
                catch
                {
                    ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Не удалось удалить пользователя", "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning);
                }
                RefreshCollection();
            }
        }

        #endregion
    }
}
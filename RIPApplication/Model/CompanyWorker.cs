﻿using RIPApplication.Classes;
using RIPApplication.Classes.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIPApplication.Model
{
    class CompanyWorker
    {
        public event Action Loading;

        public event Action Loaded;

        public readonly ObservableCollection<Company> companies = new ObservableCollection<Company>();

        public CompanyWorker()
        {

        }

        public void AddCompany()
        {
            ServiceContainer.Instance.GetService<IAddEditService>().CompanyShow();
            RefreshCollection();
        }

        public void EditCompany(Company company)
        {
            if (company != null)
            {
                ServiceContainer.Instance.GetService<IAddEditService>().CompanyShow(company);
                RefreshCollection();
            }
        }

        public async Task DeleteCompany(Company company)
        {
            if (company != null)
            {
                try
                {
                    var result = ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Вы действительно хотите удалить компанию из базы?", "Внимание", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Question);
                    if (result != System.Windows.MessageBoxResult.Yes)
                        return;
                    await company.Delete();
                }
                catch
                {
                    ServiceContainer.Instance.GetService<IMessageBoxService>()
                        .Show("Не удалось удалить компанию", "Ошибка", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Warning);
                }
                RefreshCollection();
            }
        }

        public void RefreshCollection()
        {
            Loading?.Invoke();
            companies.Clear();

            using (var companyContext = new RIPContext())
            {
                companyContext.Configuration.LazyLoadingEnabled = false;
                companyContext.Companies.Load();
                foreach (var company in companyContext.Companies)
                    companies.Add(company);
            }

            Loaded?.Invoke();
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.Entity;

namespace RIPApplication.Model
{
    class RIPInitializer : DropCreateDatabaseAlways<RIPContext>
    {
        protected override void Seed(RIPContext context)
        {
            var defaultCompany = new List<Company>
            {
                new Company() { Id = 1, Name = "ООО «Какие Люди»", ContractStatus = ContractStatus.Concluded },
                new Company() { Id = 2, Name = "Рога и Копыта", ContractStatus = ContractStatus.Broken },
                new Company() { Id = 3, Name = "ООО «Зеленоглазое Такси ООО»", ContractStatus = ContractStatus.NotConcluded }
            };

            var defaultUser = new List<User>
            {
                new User() { Id = 1, Name = "Марьиванна", Login = "marivan", Password = "123qweasd", Company = defaultCompany[0] },
                new User() { Id = 2, Name = "Иванов Иван Иванович", Login = "ivanivanivan", Password = "answer42", Company = defaultCompany[0] },
                new User() { Id = 3, Name = "Вовочка", Login = "vova", Password = "¯\\_(ツ)_/¯", Company = defaultCompany[1] }
            };

            context.Companies.AddRange(defaultCompany);
            context.Users.AddRange(defaultUser);

            base.Seed(context);
        }
    }
}
﻿using System.Data.Entity;


namespace RIPApplication.Model
{
    public class RIPContext : DbContext
    {
        public RIPContext() : base("RIP")
        {
            if (!Database.Exists())
                Database.SetInitializer(new RIPInitializer());
        }

        public DbSet<Company> Companies { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasMany(c => c.Users).WithRequired(x => x.Company);
        }
    }
}
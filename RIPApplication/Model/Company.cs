﻿using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Threading.Tasks;

namespace RIPApplication.Model
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; } = "";
        public ContractStatus ContractStatus { get; set; } = ContractStatus.NotConcluded;

        public virtual ObservableCollection<User> Users { get; private set; } = new ObservableCollection<User>();

        #region Public Method

        public async Task Add()
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Companies.Add(this);
                await companyContext.SaveChangesAsync();
            }
        }

        public async Task Update()
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Entry<Company>(this).State = EntityState.Modified;
                await companyContext.SaveChangesAsync();
            }
        }

        public async Task Delete()
        {
            using (var companyContext = new RIPContext())
            {
                companyContext.Companies.Attach(this);
                companyContext.Companies.Remove(this);
                await companyContext.SaveChangesAsync();
            }
        }

        #endregion
    }
}
﻿using RIPApplication.Model;

namespace RIPApplication.Classes.Services
{
    interface IAddEditService
    {
        void CompanyShow();
        void CompanyShow(Company company);
        void UserShow(Company company);
        void UserShow(User user);
    }
}
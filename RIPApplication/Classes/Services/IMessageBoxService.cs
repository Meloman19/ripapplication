﻿using System.Windows;

namespace RIPApplication.Classes.Services
{
    interface IMessageBoxService
    {
        MessageBoxResult Show(string text, string caption, MessageBoxButton buttons, MessageBoxImage image);
    }
}
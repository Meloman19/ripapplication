﻿using System.Windows;

namespace RIPApplication.Classes.Services
{
    class MessageBoxService : IMessageBoxService
    {
        MessageBoxResult IMessageBoxService.Show(string text, string caption, MessageBoxButton buttons, MessageBoxImage image)
        {
            return MessageBox.Show(text, caption, buttons, image);
        }
    }
}
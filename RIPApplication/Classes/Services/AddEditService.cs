﻿using RIPApplication.Model;
using RIPApplication.View;
using RIPApplication.ViewModel;

namespace RIPApplication.Classes.Services
{
    // Честно говоря, не вижу смысла в данном 
    // случае запариваться с мудрёнными сервисами.
    // Такой сервис хоть и "тупой", 
    // но, вроде, соответствует MVVM.
    class AddEditService : IAddEditService
    {
        public void CompanyShow()
        {
            CompanyAddEdit a = new CompanyAddEdit();
            a.DataContext = new CompanyAddEditVM();
            a.ShowDialog();
        }

        public void CompanyShow(Company company)
        {
            CompanyAddEdit a = new CompanyAddEdit();
            a.DataContext = new CompanyAddEditVM(company);
            a.ShowDialog();
        }

        public void UserShow(Company company)
        {
            UserAddEdit a = new UserAddEdit();
            a.DataContext = new UserAddEditVM(company);
            a.ShowDialog();
        }

        public void UserShow(User user)
        {
            UserAddEdit a = new UserAddEdit();
            a.DataContext = new UserAddEditVM(user);
            a.ShowDialog();
        }
    }
}
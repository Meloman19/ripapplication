﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RIPApplication.Classes
{
    class CommandAsync : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Func<Task> execute;

        public CommandAsync(Func<Task> execute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
        }

        public bool CanExecute(object parameter) => true;

        public async void Execute(object parameter) => await execute();
    }

    class CommandAsync<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Func<T, Task> execute;

        public CommandAsync(Func<T, Task> execute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
        }

        public bool CanExecute(object parameter) => true;

        public async void Execute(object parameter)
        {
            if (parameter is T par)
                await execute(par);
            else
                await execute(default(T));
        }
    }
}
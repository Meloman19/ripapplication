﻿using System;
using System.Windows.Input;

namespace RIPApplication.Classes
{
    public class CommandRelay : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action execute;

        public CommandRelay(Action execute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
        }

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter) => execute();
    }

    public class CommandRelay<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<T> execute;

        public CommandRelay(Action<T> execute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
        }

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            if (parameter is T par)
                execute(par);
            else
                execute(default(T));
        }
    }
}
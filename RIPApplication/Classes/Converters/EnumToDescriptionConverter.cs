﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.ComponentModel;
using System.Reflection;

namespace RIPApplication.Classes.Converters
{
    class EnumToDescriptionConverter : IValueConverter
    {
        private Type type;
        private object[] names;
        private Dictionary<object, object> valueToName;
        private Dictionary<object, object> nameToValue;

        public IReadOnlyCollection<object> Names { get; private set; }

        public Type Type
        {
            get => type;
            set
            {
                if (!value.IsEnum)
                    throw new Exception("Type is not enum");
                type = value;
                Init();
            }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return valueToName[value];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return nameToValue[value];
        }

        public void Init()
        {
            var fields = type.GetFields(BindingFlags.Static | BindingFlags.Public);

            valueToName = fields.ToDictionary(x => x.GetValue(null), y => GetDescription(y));

            nameToValue = fields.ToDictionary(x => GetDescription(x), y => y.GetValue(null));

            names = fields.Select(x => GetDescription(x)).ToArray();
            Names = names;
        }

        static object GetDescription(FieldInfo fieldInfo)
        {
            var description = (DescriptionAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(DescriptionAttribute));
            return description != null ? description.Description : fieldInfo.GetValue(null);
        }
    }
}
﻿namespace RIPApplication.Classes
{
    interface ICloseable
    {
        void Close();
    }
}
﻿using RIPApplication.Model;
using RIPApplication.View;
using RIPApplication.ViewModel;
using System.Windows;

namespace RIPApplication
{
    public partial class App : Application
    {
        public App()
        {
            ServiceInjector.InjectServices();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindow = new MainWindow() { DataContext = new MainWindowVM() };
            MainWindow.Show();
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
        }
    }
}